﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProviderDataImport.AppModels;
using System.Net;
using Data.Models;
using LoggingService;

namespace ProviderDataImport.Services
{
    public class ImportDataService
    {
        private readonly IConfiguration _config;

        private readonly LoggerService _logger;
        private readonly HospitalDataService _hospitalDataService;
        private readonly LanguageDataService _languageDataService;
        private readonly SpecialtyDataService _specialtyDataService;
        private readonly DataService.DataService _apiDataService;
        public ImportDataService(IConfiguration config)
        {
            _logger = new LoggerService("DataService");
            _config = config;

            _apiDataService = new DataService.DataService();
            _hospitalDataService = new HospitalDataService(_config);
            _languageDataService = new LanguageDataService(_config);
            _specialtyDataService = new SpecialtyDataService(_config);
        }

        /// <summary>
        /// Get All Providers from Microservice 
        /// </summary>
        /// <returns>List<MarketingProvider></returns>
        public async Task<List<MarketingProvider>?> GetProviders()
        {
            try
            {
                var serviceUri = $"{_config["AppSettings:ProvidersUrl"]}";
                var response = await _apiDataService.GetAsync(serviceUri);
                if (response.StatusCode != HttpStatusCode.OK) return null;
                var apiResult = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<MarketingProvider>>(apiResult);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: DataService::GetProviders() :: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Remove Providers from db
        /// <param name="marketingProviders">List of providers</param>
        /// </summary>
        public async Task RemoveProviders(List<MarketingProvider> marketingProviders, List<Provider> dbProviders)
        {
            #region DeleteProviders
            var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
           
            await Parallel.ForEachAsync(dbProviders, options, async (provider, ct) =>
            {
                await using var db = new ProviderDataDBContext(_config["ConnectionStrings:DefaultConnection"]);
                var providerDataService = new ProviderDataService();
                try
                {
                    var exists = marketingProviders.Any(x => x.EchoDoctorNumber == provider.EchoDoctorNumber);
                    if (!exists)
                    {
                        _logger.WriteInfo(provider.FirstName + " " + provider.LastName + " removed.");
                        Console.WriteLine(provider.FirstName + " " + provider.LastName + " removed. ");
                        await providerDataService.RemoveProviderFromDb(provider, db);
                    }

                    if ((provider.IsArchived != null && (bool) provider.IsArchived) || (provider.IsHide != null && (bool)provider.IsHide) || (provider.AdminApproved != null && !(bool)provider.AdminApproved))
                    {
                        _logger.WriteInfo(provider.FirstName + " " + provider.LastName + " removed. IsArchived=" +
                                          provider.IsArchived + " IsHide=" + provider.IsHide + " AdminApproved=" +
                                          provider.AdminApproved);
                        Console.WriteLine(provider.FirstName + " " + provider.LastName + " removed. IsArchived=" +
                                          provider.IsArchived + " IsHide=" + provider.IsHide + " AdminApproved=" +
                                          provider.AdminApproved);

                        await providerDataService.RemoveProviderFromDb(provider, db);
                    }
                }
                catch (Exception e)
                {
                    _logger.WriteError("Error Removing provider " + provider.FirstName + " " +
                                       provider.LastName + " :: Error: " + e.Message);
                    Console.WriteLine("Error Removing provider " + provider.FirstName + " " +
                                       provider.LastName + " :: Error: " + e.Message);
                }
            });
            _logger.WriteInfo("Remove providers finished at:: " + DateTime.Now);
            Console.WriteLine("Remove providers finished at:: " + DateTime.Now);
            #endregion
        }

        /// <summary>
        /// Imports Providers into db
        /// <param name="marketingProviders">List of providers</param>
        /// </summary>
        public async Task ImportProviderData(List<MarketingProvider> marketingProviders)
        {
            var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
            try
            {
                #region ImportProviderData
                _logger.WriteInfo("update providers starting at:: " + DateTime.Now);
                Console.WriteLine("update providers starting at:: " + DateTime.Now);

                await Parallel.ForEachAsync(marketingProviders, options, async (provider, ct) =>
                {
                    await using var db = new ProviderDataDBContext(_config["ConnectionStrings:DefaultConnection"]);
                    try
                    {
                        var providerDataService = new ProviderDataService();
                        await providerDataService.UpdateProviderData(provider, db);
                        _logger.WriteInfo("Transaction completed for " + provider.FirstName + " " + provider.LastName);
                        Console.WriteLine("Transaction completed for " + provider.FirstName + " " + provider.LastName);
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Error Updating/Inserting provider " + provider.FirstName + " " +
                                           provider.LastName + " :: Error: " + e.Message);
                        Console.WriteLine("Error Updating/Inserting provider " + provider.FirstName + " " +
                                          provider.LastName + " :: Error: " + e.Message);
                    }

                });

                _logger.WriteInfo("====================================================================");
                _logger.WriteInfo("update providers finished at:: " + DateTime.Now);
                Console.WriteLine("====================================================================");
                Console.WriteLine("update providers finished at:: " + DateTime.Now);
                #endregion

                await _hospitalDataService.ImportHospitalData(marketingProviders);

                await _languageDataService.ImportLanguageData();

                await _specialtyDataService.ImportSpecialtyData();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: DataService::ImportProviderData() :: {e.Message}");
                throw;
            }
        }
    }
    
}
