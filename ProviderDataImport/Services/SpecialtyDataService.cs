﻿using Data.Models;
using LoggingService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SendMailService.Services;

//using ProviderDataImport.Helper;

namespace ProviderDataImport.Services
{
    public class SpecialtyDataService
    {
        private readonly LoggerService _logger;
        private readonly IConfiguration _config;
        private readonly DataService.DataService _dataService;
        private readonly string _specialtyUrl;
        private static SendMail _sendMail = null!;
        private static string _to = null!;

        public SpecialtyDataService(IConfiguration config)
        {
            _config = config;
            _logger = new LoggerService("SpecialtyDataService");
            _dataService = new DataService.DataService();
            _specialtyUrl = _config.GetSection("AppSettings").GetSection("SpecialtyUrl").Value;
            _to = _config.GetSection("AppSettings").GetSection("recipient").Value;
            _sendMail = new SendMail(_config);
        }

        /// <summary>
        /// Imports Specialty Data into db
        /// </summary>
        public async Task ImportSpecialtyData()
        {
            try
            {
                var response = await _dataService.GetAsync(_specialtyUrl);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    _logger.WriteInfo("Specialties received successfully .........");
                    var specialties = JsonConvert.DeserializeObject<List<Specialty>>(content);
                    var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
                    if (specialties != null)
                        await Parallel.ForEachAsync(specialties, options, async (specialty, ct) =>
                        {
                            await using var dbContext = new ProviderDataDBContext(_config["ConnectionStrings:DefaultConnection"]);
                            try
                            {
                                var s = await dbContext.Specialties.Where(x => x.Code == specialty.Code).FirstOrDefaultAsync(cancellationToken: ct);

                                if (s != null)
                                {
                                    s.Code = specialty.Code;
                                    s.Text = specialty.Text;
                                    s.DropDownValue = specialty.DropDownValue;
                                    s.JobTitle = specialty.JobTitle;
                                    s.CertifyingBoardName = specialty.CertifyingBoardName;
                                    s.BoardCertification = specialty.BoardCertification;
                                    s.BoardDescription = specialty.BoardDescription;
                                    s.UpdateDate = DateTime.Now;
                                    s.RecentUser = "UpdateIMPORT";
                                    _logger.WriteInfo($"Specialty {specialty.Text} updated");
                                    Console.WriteLine($"Specialty {specialty.Text} updated");
                                }
                                else
                                {
                                    var newSpecialty = new Specialty
                                    {
                                        Code = specialty.Code,
                                        Text = specialty.Text,
                                        DropDownValue = specialty.DropDownValue,
                                        JobTitle = specialty.JobTitle,
                                        CertifyingBoardName = specialty.CertifyingBoardName,
                                        BoardCertification = specialty.BoardCertification,
                                        BoardDescription = specialty.BoardDescription,
                                        InsertDate = DateTime.Now,
                                        RecentUser = "InsertImport"
                                    };
                                    dbContext.Specialties.Add(newSpecialty);
                                    _logger.WriteInfo("New Specialty being added " + newSpecialty.Text);
                                    Console.WriteLine($"New Specialty {specialty.Text} added");
                                }

                                await dbContext.SaveChangesAsync(ct);
                            }
                            catch (Exception e)
                            {
                                _logger.WriteError("Exception Occurred inserting/updating Specialties :: " + e.Message +
                                              " " + e.StackTrace);
                            }
                        });
                }
                else
                {
                    _logger.WriteError($"Error retrieving Specialties {response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Exception Occurred in GetSpecialties() " + e.Message + " " + e.StackTrace);

                var body = e.Message;
                var subject = "Provider Data Import Exception GetSpecialties()....";

                await _sendMail.Send(body, _to, null, subject);
            }
        }
    }
}
