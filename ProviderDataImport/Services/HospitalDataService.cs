﻿using Data.Models;
using LoggingService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProviderDataImport.AppModels;

namespace ProviderDataImport.Services
{
    public class HospitalDataService
    {
        private readonly LoggerService _logger;
        private readonly IConfiguration _config;
        private readonly string _hospitalUrl;
        private readonly DataService.DataService _apiDataService;
        public HospitalDataService(IConfiguration config)
        {
            _config = config;
            _logger = new LoggerService("HospitalDataService");
            _hospitalUrl = _config.GetSection("AppSettings").GetSection("HospitalUrl").Value;
            _apiDataService = new DataService.DataService();
        }

        /// <summary>
        /// Import Hospital Data into db
        /// <param name="marketingProviders">List of providers</param>
        /// </summary>
        public async Task ImportHospitalData(List<MarketingProvider> marketingProviders)
        {
            var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
            try
            {
                #region HospitalMapping
                _logger.WriteInfo("Hospitals starting at:: " + DateTime.Now);
                Console.WriteLine("Hospitals starting at:: " + DateTime.Now);
                var response = await _apiDataService.GetAsync(_hospitalUrl);
                var hospitalsJson = await response.Content.ReadAsStringAsync();
                var hospitalList = JsonConvert.DeserializeObject<List<MarketingHospitalMapping>>(hospitalsJson);
                if (hospitalList != null)
                {
                    if (response.IsSuccessStatusCode)
                    {

                        await Parallel.ForEachAsync(hospitalList, options, async (hospital, ct) =>
                        {
                            await using var db =
                                new ProviderDataDBContext(_config["ConnectionStrings:DefaultConnection"]);
                            try
                            {
                                await ProcessHospitalData(db, hospital);
                            }
                            catch (Exception e)
                            {
                                _logger.WriteError("Error Updating/Inserting Hospitals " + hospital.HospitalName +
                                                   " :: Error: " + e.Message);
                                Console.WriteLine("Error Updating/Inserting Hospitals " + hospital.HospitalName +
                                                  " :: Error: " + e.Message);
                            }
                        });
                    }
                }

                #endregion
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: HospitalDataService::ProcessHospitalData() :: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Processes the data for update/insert of hospitals
        /// <param name="hospital">List of hospitals</param>
        /// </summary>
        private async Task ProcessHospitalData(ProviderDataDBContext db, MarketingHospitalMapping hospital)
        {
            try
            {
                var hE = await db.Hospitals.FirstOrDefaultAsync(x => x.HospitalCode == hospital.HospitalCode);

                if (hE != null)
                {
                    // hE.HospitalCode = hospital.HospitalCode;
                    hE.HospitalName = hospital.HospitalName;
                    hE.StaffCode = hospital.StaffCode;
                    hE.OrderId = hospital.OrderId;
                    hE.UpdateDate = DateTime.Now;
                    hE.RecentUser = "UpdateIMPORT";
                    _logger.WriteInfo($"Updating Hospital {hE.HospitalName}");
                    Console.WriteLine($"Updating Hospital {hE.HospitalName}");
                }

                else
                {
                    var newHospital = new Hospital
                    {
                        HospitalCode = hospital.HospitalCode,
                        HospitalName = hospital.HospitalName,
                        StaffCode = hospital.StaffCode,
                        OrderId = hospital.OrderId,
                        InsertDate = DateTime.Now,
                        RecentUser = "InsertImport"
                    };
                    db.Hospitals.Add(newHospital);
                    _logger.WriteInfo($"Hospital {hospital.HospitalName} Added");
                    Console.WriteLine($"Hospital {hospital.HospitalName} added");
                }

                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Updating/Inserting Hospitals :: HospitalDataService::ImportHospitalData() :: Hospital: {hospital.HospitalName} :: Details: {e.Message}" + e.InnerException ?? "");
                throw;
            }
        }
    }
}
