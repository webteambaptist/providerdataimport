﻿using Data.Models;
using LoggingService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SendMailService.Services;

namespace ProviderDataImport.Services
{
    public class LanguageDataService
    {
        private readonly LoggerService _logger;
        private readonly IConfiguration _config;
        private static DataService.DataService _dataService = null!;
        private static string _languageUrl = null!;
        private static string _to = null!;
        private static SendMail _sendMail = null!;
        public LanguageDataService(IConfiguration config)
        {
            _config = config;
            _logger = new LoggerService("LanguageDataService");
            _dataService = new DataService.DataService();
            _languageUrl = _config.GetSection("AppSettings").GetSection("LanguageUrl").Value;
            _sendMail = new SendMail(_config);
            _to = _config.GetSection("AppSettings").GetSection("recipient").Value;
        }

        /// <summary>
        /// Imports Languages into db
        /// </summary>
        public async Task ImportLanguageData()
        {
            _logger.WriteInfo("Started Getting Languages ... ");
            Console.WriteLine("Started Getting Languages ... ");
            try
            {
                _logger.WriteInfo("Started Getting Languages ... ");
                Console.WriteLine("Started Getting Languages ... ");
                var response = await _dataService.GetAsync(_languageUrl);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    _logger.WriteInfo("Languages received successfully .........");
                    Console.WriteLine("Languages received successfully .........");
                    var languages = JsonConvert.DeserializeObject<List<Language>>(content);
                    var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
                    if (languages != null)
                        await Parallel.ForEachAsync(languages, options, async (lang, ct) =>
                        {
                            await using var dbContext =
                                new ProviderDataDBContext(_config["ConnectionStrings:DefaultConnection"]);
                            try
                            {
                                var l = await dbContext.Languages.Where(x => x.Code == lang.Code)
                                    .FirstOrDefaultAsync(cancellationToken: ct);

                                if (l != null)
                                {
                                    l.Text = lang.Text;
                                    l.UpdateDate = DateTime.Now;
                                    l.RecentUser = "UpdateIMPORT";
                                    _logger.WriteInfo($"Language {l.Text} updated");
                                    Console.WriteLine($"Language {l.Text} updated");
                                }
                                else
                                {
                                    var newLanguage = new Language
                                    {
                                        Code = lang.Code,
                                        Text = lang.Text,
                                        InsertDate = DateTime.Now,
                                        RecentUser = "InsertImport"
                                    };
                                    dbContext.Languages.Add(newLanguage);
                                    _logger.WriteInfo("New Language being added " + newLanguage.Text);
                                    Console.WriteLine("New Language being added " + newLanguage.Text);
                                }

                                await dbContext.SaveChangesAsync(ct);
                            }
                            catch (Exception e)
                            {
                                _logger.WriteError(
                                    $"Exception Occurred inserting/updating Languages :: {e.Message} : {e.StackTrace} ");
                                Console.WriteLine(
                                    $"Exception Occurred inserting/updating Languages :: {e.Message} : {e.StackTrace} ");
                            }
                        });
                }
                _logger.WriteInfo("Finished Getting Languages from Service");
                Console.WriteLine("Finished Getting Languages from Service");
            }
            catch (Exception e)
            {
                _logger.WriteError("Exception Occurred in GetLanguages() " + e.Message + " " + e.StackTrace);
                Console.WriteLine("Exception Occurred in GetLanguages() " + e.Message + " " + e.StackTrace);

                var body = e.Message;
                var subject = "Provider Data Import Exception GetLanguages()....";
                await _sendMail.Send(body, _to, null, subject);
            }
        }
    }
}
