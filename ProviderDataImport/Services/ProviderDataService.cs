﻿using Data.Models;
using LoggingService;
using Microsoft.EntityFrameworkCore;
using ProviderDataImport.AppModels;
using Type = Data.Models.Type;

namespace ProviderDataImport.Services
{
    public class ProviderDataService
    {
        private readonly LoggerService _logger;
     
        public ProviderDataService()
        {
            _logger = new LoggerService("ProviderDataService");
        }

        /// <summary>
        /// Removes provider from db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        public async Task RemoveProviderFromDb(Provider provider, ProviderDataDBContext dbContext)
        {
            _logger.WriteInfo("====================================================================");
            _logger.WriteInfo("Checking for Providers that need to be removed from db.");
            try
            {
                try
                {
                    //Provider
                    Console.WriteLine($"Start Removing " + provider.FirstName + " " + provider.LastName);
                    dbContext.Remove(provider);
                    Console.WriteLine($"EndRemoving " + provider.FirstName + " " + provider.LastName);
                    await dbContext.SaveChangesAsync();
                    // if I fail to delete details but this one commits then we have a bunch of bad data. 
                    _logger.WriteInfo("Provider Removed from db. :: Provider Name: " + provider.FirstName + " " +
                                      provider.LastName);
                }
                catch (Exception ex)
                {
                    _logger.WriteError("There was an issue removing record :: " + ex.Message + " ProviderGuid: " +
                                       provider.Guid);
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("There was an issue retrieving Providers from db. Exception :: " + e.Message);
            }
        }

        /// <summary>
        /// Update provider in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        public async Task UpdateProviderData(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                await ProcessProviderDataUpdate(provider, dbContext);

                await ProcessProviderTypeUpdate(provider, dbContext);
                
                await ProcessGroupMappingUpdate(provider, dbContext);

                await ProcessSpecialtyMappingUpdate(provider, dbContext);

                await ProcessLanguageMappingUpdate(provider, dbContext);

                await ProcessEducationMappingUpdate(provider, dbContext);

                await ProcessSuffixMappingUpdate(provider, dbContext);

                await ProcessHospitalMappingUpdate(provider, dbContext);

                await ProcessAgesTreatedMappingUpdate(provider, dbContext);

                await ProcessPatientFormsMappingUpdate(provider, dbContext);

                await ProcessLocationMappingUpdate(provider, dbContext);

                await ProcessBoardMappingUpdate(provider, dbContext);

                await ProcessLicenseMappingUpdate(provider, dbContext);

                await ProcessExpertiseUpdate(provider, dbContext);

                await ProcessProviderParagraphsUpdate(provider, dbContext);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ProviderDataService.UpdateProviderData() :: Provider: {provider.FirstName} {provider.LastName} :: Details: {e.Message}" + e.InnerException ?? "");
                throw;
            }
        }

        /// <summary>
        /// Updates Main Provider info in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessProviderDataUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                var result = await dbContext.Providers.FirstOrDefaultAsync(x => x.Guid == provider.Guid);

                if (result != null)
                {
                    result.FirstName = provider.FirstName;
                    result.MiddleName = provider.MiddleName;
                    result.LastName = provider.LastName;
                    result.FormattedCommonName = provider.FormattedCommonName;
                    result.FormattedLastNameFirst = provider.FormattedLastNameFirst;
                    result.EchoPhysicianId = provider.EchoPhysicianId;
                    result.EchoDoctorNumber = provider.EchoDoctorNumber;
                    result.NationalId = provider.NationalId;
                    result.ContactPhone = provider.ContactPhone;
                    result.ContactCellPhone = provider.ContactCellPhone;
                    result.ContactPager = provider.ContactPager;
                    result.GenderId = provider.GenderId;
                    result.DoctorImage = provider.DoctorImage;
                    result.DoctorHeaderImage = provider.DoctorHeaderImage;
                    result.ThumbnailPhoto = provider.ThumbnailPhoto;
                    result.EchoSuffix = provider.EchoSuffix;
                    result.UrlRoute = provider.UrlRoute;
                    result.FormattedUrlRoute = provider.FormattedUrlRoute;
                    result.IsAcceptingNewPatients = provider.IsAcceptingNewPatients;
                    result.IsAcceptingNewPatientsVisible = provider.IsAcceptingNewPatientsVisible;
                    result.TypeId = provider.TypeId;
                    result.Introduction = provider.Introduction;
                    result.FormattedSpecialties = provider.FormattedSpecialties;
                    result.IsArchived = provider.IsArchived;
                    result.IsHide = provider.IsHide;
                    result.NameOverride = provider.NameOverride;
                    result.JobTitleOverride = provider.JobTitleOverride;
                    result.FormattedJobTitle = provider.FormattedJobTitle;
                    result.LegalPracticeName = provider.LegalPracticeName;
                    result.DoesAcceptsEapps = provider.DoesAcceptsEapps;
                    result.IsPcmh = provider.IsPcmh;
                    result.IsBloodless = provider.IsBloodless;
                    result.AdminApproved = provider.AdminApproved;
                    result.IsNewDoctor = provider.IsNewDoctor;
                    result.IsBpp = provider.IsBpp;
                    result.IsEmployed = provider.IsEmployed;
                    result.DoesHavePatientReviews = provider.DoesHavePatientReviews;
                    result.PublicationQuery = provider.PublicationQuery;
                    result.LastUpdateDateTime = provider.LastUpdateDateTime;
                    result.LastUpdateUser = provider.LastUpdateUser;
                    result.UpdateDate = DateTime.Now;
                    result.RecentUser = "UpdateIMPORT";
                    _logger.WriteInfo("Provider being updated " + provider.FirstName + " " + provider.LastName);
                }
                else
                {
                    var newProvider = new Provider
                    {
                        Id = provider.Id,
                        Guid = provider.Guid,
                        FirstName = provider.FirstName,
                        MiddleName = provider.MiddleName,
                        LastName = provider.LastName,
                        FormattedCommonName = provider.FormattedCommonName,
                        FormattedLastNameFirst = provider.FormattedLastNameFirst,
                        EchoPhysicianId = provider.EchoPhysicianId,
                        EchoDoctorNumber = provider.EchoDoctorNumber,
                        NationalId = provider.NationalId,
                        ContactPhone = provider.ContactPhone,
                        ContactCellPhone = provider.ContactCellPhone,
                        ContactPager = provider.ContactPager,
                        GenderId = provider.GenderId,
                        DoctorImage = provider.DoctorImage,
                        DoctorHeaderImage = provider.DoctorHeaderImage,
                        ThumbnailPhoto = provider.ThumbnailPhoto,
                        EchoSuffix = provider.EchoSuffix,
                        UrlRoute = provider.UrlRoute,
                        FormattedUrlRoute = provider.FormattedUrlRoute,
                        IsAcceptingNewPatients = provider.IsAcceptingNewPatients,
                        IsAcceptingNewPatientsVisible = provider.IsAcceptingNewPatientsVisible,
                        TypeId = provider.TypeId,
                        Introduction = provider.Introduction,
                        FormattedSpecialties = provider.FormattedSpecialties,
                        IsArchived = provider.IsArchived,
                        IsHide = provider.IsHide,
                        NameOverride = provider.NameOverride,
                        JobTitleOverride = provider.JobTitleOverride,
                        FormattedJobTitle = provider.FormattedJobTitle,
                        LegalPracticeName = provider.LegalPracticeName,
                        DoesAcceptsEapps = provider.DoesAcceptsEapps,
                        IsPcmh = provider.IsPcmh,
                        IsBloodless = provider.IsBloodless,
                        AdminApproved = provider.AdminApproved,
                        IsNewDoctor = provider.IsNewDoctor,
                        IsBpp = provider.IsBpp,
                        IsEmployed = provider.IsEmployed,
                        DoesHavePatientReviews = provider.DoesHavePatientReviews,
                        PublicationQuery = provider.PublicationQuery,
                        LastUpdateDateTime = provider.LastUpdateDateTime,
                        LastUpdateUser = provider.LastUpdateUser,
                        InsertDate = DateTime.Now,
                        RecentUser = "InsertImport"
                    };
                    _logger.WriteInfo("New Provider being added " + provider.FirstName + " " + provider.LastName);

                    dbContext.Providers.Add(newProvider);
                }

                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Updating/Inserting provider :: ProviderDataService::ProcessProviderDataUpdate() :: Provider: {provider.FirstName} {provider.LastName} :: Details: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Updates Provider Type in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessProviderTypeUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                var result = await dbContext.Types.FirstOrDefaultAsync(x => x.ProviderGuid == provider.Guid);
                if (result != null)
                {
                    result.Id = provider.Type.Id;
                    result.Code = provider.Type.Code;
                    result.Text = provider.Type.Text;
                    result.UpdateDate = DateTime.Now;
                    result.RecentUser = "UpdateIMPORT";
                    Console.WriteLine("Provider Type being updated " + provider.FirstName + " " + provider.LastName);
                    _logger.WriteInfo("Provider Type being updated " + provider.FirstName + " " + provider.LastName);
                }
                else
                {
                    var newType = new Type
                    {
                        ProviderGuid = provider.Guid,
                        Id = provider.Type.Id,
                        Code = provider.Type.Code,
                        Text = provider.Type.Text,
                        InsertDate = DateTime.Now,
                        RecentUser = "InsertImport"
                    };
                    Console.WriteLine("New Type for Provider being added " + provider.FirstName + " " + provider.LastName);
                    _logger.WriteInfo("New Type for Provider being added " + provider.FirstName + " " + provider.LastName);
                    dbContext.Types.Add(newType);
                }

                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Updating/Inserting type provider :: ProviderDataService.ProcessProviderTypeUpdate() :: Provider: {provider.FirstName} {provider.LastName} :: Details: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Updates Provider Group Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessGroupMappingUpdate( MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first delete any groupsMapping that doesn't exist from Marketing
                var providerGroupsMapping = await dbContext.GroupsMappings.Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var providerGroup in providerGroupsMapping.Where(providerGroup =>
                                 !provider.GroupsMapping.Select(x => x.Code).Contains(providerGroup.Code)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing GroupsMapping {providerGroup.Text} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing GroupsMapping {providerGroup.Text} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(providerGroup);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing Groups Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Groups Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }


                // now update/add
                foreach (var groupsMapping in provider.GroupsMapping)
                {
                    try
                    {
                        var gm = await dbContext.GroupsMappings.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid && x.Code == groupsMapping.Code);

                        if (gm != null)
                        {
                            gm.Code = groupsMapping.Code;
                            gm.Text = groupsMapping.Text;
                            gm.Type = groupsMapping.Type;
                            gm.OrderId = groupsMapping.OrderId;
                            gm.IsPrimary = groupsMapping.IsPrimary;
                            gm.UpdateDate = DateTime.Now;
                            gm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newGroupMapping = new GroupsMapping
                            {
                                ProviderGuid = provider.Guid,
                                Code = groupsMapping.Code,
                                Text = groupsMapping.Text,
                                Type = groupsMapping.Type,
                                OrderId = groupsMapping.OrderId,
                                IsPrimary = groupsMapping.IsPrimary,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };

                            dbContext.GroupsMappings.Add(newGroupMapping);
                            _logger.WriteInfo("New Group Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Group Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving GroupsMapping" + e.Message);
                        Console.WriteLine("Exception occurred saving GroupsMapping" + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting group mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting group mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Specialty Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessSpecialtyMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first delete those that no longer exist in marketing
                var dbSpecialtyMappings = await dbContext.SpecialtyMappings
                    .Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var dbSpecialtyMapping in dbSpecialtyMappings.Where(dbSpecialtyMapping => !provider.SpecialtyMapping.Select(x => x.SpecialtyCode).Contains(dbSpecialtyMapping.SpecialtyCode)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing SpecialtyMapping {dbSpecialtyMapping.Text} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing SpecialtyMapping {dbSpecialtyMapping.Text} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(dbSpecialtyMapping);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing Specialty Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Specialty Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }

                foreach (var specialtyMapping in provider.SpecialtyMapping)
                {
                    try
                    {
                        var sm = await dbContext.SpecialtyMappings.FirstOrDefaultAsync(x =>
                        x.ProviderGuid == provider.Guid &&
                        x.SpecialtyCode == specialtyMapping.SpecialtyCode);

                        if (sm != null)
                        {
                            sm.SpecialtyCode = specialtyMapping.SpecialtyCode;
                            sm.Text = specialtyMapping.Text;
                            sm.JobTitle = specialtyMapping.JobTitle;
                            sm.SpecialtyStatus = specialtyMapping.SpecialtyStatus;
                            sm.SpecialtyType = specialtyMapping.SpecialtyType;
                            sm.HippaTaxonomy = specialtyMapping.HippaTaxonomy;
                            sm.IsBoardCertified = specialtyMapping.IsBoardCertified;
                            sm.IsPrimary = specialtyMapping.IsPrimary;
                            sm.UpdateDate = DateTime.Now;
                            sm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Specialty Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Specialty Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newSpecialtyMapping = new SpecialtyMapping
                            {
                                ProviderGuid = provider.Guid,
                                SpecialtyCode = specialtyMapping.SpecialtyCode,
                                Text = specialtyMapping.Text,
                                JobTitle = specialtyMapping.JobTitle,
                                SpecialtyStatus = specialtyMapping.SpecialtyStatus,
                                SpecialtyType = specialtyMapping.SpecialtyType,
                                HippaTaxonomy = specialtyMapping.HippaTaxonomy,
                                IsBoardCertified = specialtyMapping.IsBoardCertified,
                                IsPrimary = specialtyMapping.IsPrimary,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };

                            dbContext.SpecialtyMappings.Add(newSpecialtyMapping);
                            _logger.WriteInfo("New Specialty Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Specialty Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving SpecialtyMapping" + e.Message);
                        Console.WriteLine("Exception occurred saving SpecialtyMapping" + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting specialty mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting specialty mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);

            }
        }

        /// <summary>
        /// Updates Provider Language Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessLanguageMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first delete what no longer exists in marketing
                var dbLanguageMapping = await dbContext.LanguageMappings.Where(x => x.ProviderGuid == provider.Guid)
                    .ToListAsync();
                foreach (var languageMapping in dbLanguageMapping.Where(languageMapping => !provider.LanguageMapping.Select(x => x.LanguageCode).Contains(languageMapping.LanguageCode)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing LanguageMapping {languageMapping.Text} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing LanguageMapping {languageMapping.Text} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(languageMapping);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing Language Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Language Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }

                foreach (var languageMapping in provider.LanguageMapping)
                {
                    try
                    {
                        var lm = await dbContext.LanguageMappings.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid &&
                            x.LanguageCode == languageMapping.LanguageCode);

                        if (lm != null)
                        {
                            lm.LanguageCode = languageMapping.LanguageCode;
                            lm.Text = languageMapping.Text;
                            lm.UpdateDate = DateTime.Now;
                            lm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Language Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Language Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newLanguageMapping = new LanguageMapping
                            {
                                ProviderGuid = provider.Guid,
                                LanguageCode = languageMapping.LanguageCode,
                                Text = languageMapping.Text,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };

                            dbContext.LanguageMappings.Add(newLanguageMapping);
                            _logger.WriteInfo("New Language Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Language Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving LanguageMapping" + e.Message);
                        Console.WriteLine("Exception occurred saving LanguageMapping" + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting language mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting language mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Education Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessEducationMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            // education should never be removed only added and updated I would think.
            // I'll add deletes just to account for mistakes
            var dbEducationMapping = await dbContext.EducationMappings.Where(x => x.ProviderGuid == provider.Guid)
                .ToListAsync();
            foreach (var educationMapping in dbEducationMapping.Where(educationMapping => !provider.EducationMapping.Select(x => x.DegreeCode)
                         .Contains(educationMapping.DegreeCode)))
            {
                try
                {
                    _logger.WriteInfo($"Removing Education Mapping {educationMapping.DegreeText} for Provider {provider.FirstName} {provider.LastName}");
                    Console.WriteLine($"Removing Education Mapping {educationMapping.DegreeText} for Provider {provider.FirstName} {provider.LastName}");
                    dbContext.Remove(educationMapping);
                    await dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    _logger.WriteError($"Error removing Education Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    Console.WriteLine($"Error removing Education Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                }
            }
            foreach (var educationMapping in provider.EducationMapping)
            {
                try
                {
                    var em = await dbContext.EducationMappings.FirstOrDefaultAsync(x =>
                    x.ProviderGuid == provider.Guid &&
                    x.ProgramCode == educationMapping.ProgramCode &&
                    x.InsitutionCode == educationMapping.InsitutionCode);

                    if (em != null)
                    {
                        em.DegreeCode = educationMapping.DegreeCode;
                        em.DegreeText = educationMapping.DegreeText;
                        em.ProgramCode = educationMapping.ProgramCode;
                        em.ProgramText = educationMapping.ProgramText;
                        em.InsitutionCode = educationMapping.InsitutionCode;
                        em.InstitutionText = educationMapping.InstitutionText;
                        em.InstitutionCity = educationMapping.InstitutionCity;
                        em.InstitutionState = educationMapping.InstitutionState;
                        em.GraduateComplete = educationMapping.Graduate_Complete;
                        em.StartYear = educationMapping.StartYear;
                        em.EndYear = educationMapping.EndYear;
                        em.SequenceId = educationMapping.SequenceId;
                        em.UpdateDate = DateTime.Now;
                        em.RecentUser = "UpdateIMPORT";

                        _logger.WriteInfo("Provider Education Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        Console.WriteLine("Provider Education Mapping being updated " + provider.FirstName +
                                          " " + provider.LastName);
                    }
                    else
                    {
                        var newEducationMapping = new EducationMapping
                        {
                            ProviderGuid = provider.Guid,
                            DegreeCode = educationMapping.DegreeCode,
                            DegreeText = educationMapping.DegreeText,
                            ProgramCode = educationMapping.ProgramCode,
                            ProgramText = educationMapping.ProgramText,
                            InsitutionCode = educationMapping.InsitutionCode,
                            InstitutionText = educationMapping.InstitutionText,
                            InstitutionCity = educationMapping.InstitutionCity,
                            InstitutionState = educationMapping.InstitutionState,
                            GraduateComplete = educationMapping.Graduate_Complete,
                            StartYear = educationMapping.StartYear,
                            EndYear = educationMapping.EndYear,
                            SequenceId = educationMapping.SequenceId,
                            InsertDate = DateTime.Now,
                            RecentUser = "InsertImport"
                        };

                        dbContext.EducationMappings.Add(newEducationMapping);
                        _logger.WriteInfo("New Education Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        Console.WriteLine("New Education Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                    }

                    await dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    _logger.WriteError("Exception occurred saving EducationMapping" + e.Message);
                    Console.WriteLine("Exception occurred saving EducationMapping" + e.Message);
                }
            }
        }
        /// <summary>
        /// Updates Provider Suffix Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessSuffixMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first delete any suffix that no longer exists in the marketing api
                var dbSuffixMapping = await dbContext.SuffixMappings.Where(x => x.ProviderGuid == provider.Guid)
                    .ToListAsync();
                foreach (var suffixMapping in dbSuffixMapping.Where(suffixMapping => !provider.SuffixMapping.Select(x => x.SuffixCode).Contains(suffixMapping.SuffixCode)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing SuffixMapping {suffixMapping.SuffixCode} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing SuffixMapping {suffixMapping.SuffixCode} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(suffixMapping);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing SuffixMapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing SuffixMapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var suffixMapping in provider.SuffixMapping)
                {
                    try
                    {
                        var sm = await dbContext.SuffixMappings.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid && x.SuffixCode == suffixMapping.SuffixCode);

                        if (sm != null)
                        {
                            sm.SuffixCode = suffixMapping.SuffixCode;
                            sm.OrderId = suffixMapping.OrderId;
                            sm.UpdateDate = DateTime.Now;
                            sm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Suffix Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Suffix Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newSuffixMapping = new SuffixMapping
                            {
                                ProviderGuid = provider.Guid,
                                SuffixCode = suffixMapping.SuffixCode,
                                OrderId = suffixMapping.OrderId,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.SuffixMappings.Add(newSuffixMapping);
                            _logger.WriteInfo("New Suffix Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Suffix Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving SuffixMapping" + e.Message);
                        Console.WriteLine("Exception occurred saving SuffixMapping" + e.Message);
                    }

                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting suffix mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting suffix mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Hospital Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessHospitalMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first delete any hospital mapping that no longer exists in teh marketing api
                var dbHospitalMapping = await dbContext.HospitalMappings.Where(x => x.ProviderGuid == provider.Guid)
                    .ToListAsync();
                foreach (var hospitalMapping in dbHospitalMapping.Where(hospitalMapping => !provider.HospitalMapping.Select(x => x.HospitalCode)
                             .Contains(hospitalMapping.HospitalCode)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing Hospital Mapping {hospitalMapping.HospitalName} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing Hospital Mapping {hospitalMapping.HospitalName} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(hospitalMapping);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {

                        _logger.WriteError($"Error removing Hospital Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Hospital Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var hospitalMapping in provider.HospitalMapping)
                {
                    try
                    {
                        var hm = await dbContext.HospitalMappings.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid &&
                            x.HospitalCode == hospitalMapping.HospitalCode);

                        if (hm != null)
                        {
                            hm.HospitalCode = hospitalMapping.HospitalCode;
                            hm.HospitalName = hospitalMapping.HospitalName;
                            hm.StaffCode = hospitalMapping.StaffCode;
                            hm.OrderId = hospitalMapping.OrderId;
                            hm.UpdateDate = DateTime.Now;
                            hm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Hospital Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Hospital Mapping being updated " + provider.FirstName +
                                              " " + provider.LastName);
                        }

                        else
                        {
                            var newHospitalMapping = new HospitalMapping
                            {
                                ProviderGuid = provider.Guid,
                                HospitalCode = hospitalMapping.HospitalCode,
                                HospitalName = hospitalMapping.HospitalName,
                                StaffCode = hospitalMapping.StaffCode,
                                OrderId = hospitalMapping.OrderId,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.HospitalMappings.Add(newHospitalMapping);
                            _logger.WriteInfo("New Hospital Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Hospital Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving Hospital Mapping" + e.Message);
                        Console.WriteLine("Exception occurred saving Hospital Mapping" + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting hospital mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting hospital mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Ages Treated Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessAgesTreatedMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first remove ages treated that no longer exists in marketing data
                var dbAgesTreated = await dbContext.AgesTreatedMappings.Where(x => x.ProviderGuid == provider.Guid)
                    .ToListAsync();
                
                foreach (var agesTreated in dbAgesTreated)
                {
                    try
                    {
                        if (!provider.AgesTreatedMappings.Any(x => x.Text == agesTreated.Text))
                        {
                            _logger.WriteInfo(
                                $"Removing AgesTreatedMapping {agesTreated.Text} for Provider {provider.FirstName} {provider.LastName}");
                            Console.WriteLine(
                                $"Removing AgesTreatedMapping {agesTreated.Text} for Provider {provider.FirstName} {provider.LastName}");
                            dbContext.Remove(agesTreated);
                            await dbContext.SaveChangesAsync();
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing Ages Treated Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Ages Treated Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var agesTreatedMapping in provider.AgesTreatedMappings)
                {
                    try
                    {
                        var aTm = await dbContext.AgesTreatedMappings.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid && x.Text == agesTreatedMapping.Text);

                        if (aTm != null)
                        {
                            aTm.Text = agesTreatedMapping.Text;
                            aTm.OrderId = agesTreatedMapping.OrderId;
                            aTm.UpdateDate = DateTime.Now;
                            aTm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Ages Treated Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Ages Treated Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newAgesTreatedMapping = new AgesTreatedMapping
                            {
                                ProviderGuid = provider.Guid,
                                Text = agesTreatedMapping.Text,
                                OrderId = agesTreatedMapping.OrderId,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.AgesTreatedMappings.Add(newAgesTreatedMapping);
                            _logger.WriteInfo("New Ages Treated Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Ages Treated Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving AgesTreatedMappings" + e.Message);
                        Console.WriteLine("Exception occurred saving AgesTreatedMappings" + e.Message);
                    }

                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting ages treated mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting ages treated mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Patient Forms Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessPatientFormsMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first delete any patient forms that don't exist in the marketing api
                var dbPatientFormsMapping = await dbContext.PatientFormsMappings
                    .Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var patientForms in dbPatientFormsMapping.Where(patientForms => !provider.PatientFormsMappings.Select(x => x.Name).Contains(patientForms.Name)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing PatientForms {patientForms.Name} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing PatientForms {patientForms.Name} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(patientForms);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing PatientForms for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing PatientForms for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var patientFormsMapping in provider.PatientFormsMappings)
                {
                    try
                    {
                        var pfm = await dbContext.PatientFormsMappings.FirstOrDefaultAsync(x =>
                        x.ProviderGuid == provider.Guid && x.Name == patientFormsMapping.Name);

                        if (pfm != null)
                        {
                            pfm.Name = patientFormsMapping.Name;
                            pfm.Type = patientFormsMapping.Type;
                            pfm.Link = patientFormsMapping.Link;
                            pfm.Description = patientFormsMapping.Description;
                            pfm.OrderId = patientFormsMapping.OrderId;
                            pfm.UpdateDate = DateTime.Now;
                            pfm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Patient Forms Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Patient Forms Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newPatientFormsMapping = new PatientFormsMapping
                            {
                                ProviderGuid = provider.Guid,
                                Name = patientFormsMapping.Name,
                                Type = patientFormsMapping.Type,
                                Link = patientFormsMapping.Link,
                                Description = patientFormsMapping.Description,
                                OrderId = patientFormsMapping.OrderId,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.PatientFormsMappings.Add(newPatientFormsMapping);
                            _logger.WriteInfo("New Patient Forms Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Patient Forms Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred while saving PatientFormsMapping" + e.Message);
                        Console.WriteLine("Exception occurred while saving PatientFormsMapping" + e.Message);
                    }

                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting patient forms mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting patient forms mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Location Mapping in db
        /// <param name="provider">Provider Guid Identifier</param>
        /// </summary>
        private async Task ProcessLocationMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // first we need to remove location mapping that don't exist in marketing api
                // delete location mapping that doesn't exist in the marketing api
                var dbLocationMapping = await dbContext.LocationMappings
                    .Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var mapping in dbLocationMapping)
                {
                    var address = await dbContext.Locations.Where(x => x.LocationGuid == mapping.LocationGuid)
                        .FirstOrDefaultAsync();
                    // if address is not found it should get added later
                    if (address == null) continue;
                    if (provider.Locations.Any(x => x.Address1 == address.Address1 && x.Address2 == address.Address2 && x.Address3 == address.Address3)) continue;
                    _logger.WriteInfo(
                        $"Removing Location Mapping {mapping.LocationGuid} for Provider {provider.FirstName} {provider.LastName}");
                    Console.WriteLine(
                        $"Removing Location Mapping {mapping.LocationGuid} for Provider {provider.FirstName} {provider.LastName}");
                    dbContext.Remove(mapping);
                    await dbContext.SaveChangesAsync();
                    
                }
                foreach (var providerLocation in provider.Locations)
                {
                    try
                    {
                        // if we don't get LocationId and LocationGuid from microservice than we are looking at Addresses and have to set these values 
                        // if they don't already exist
                        //Location location;
                        var locationGuid = "";
                        if (providerLocation.LocationId == 0 && providerLocation.LocationGuid == null)
                        {
                            // first check if it exists
                            var exists = await dbContext.Locations.AnyAsync(x =>
                                x.Name == providerLocation.Name &&
                                x.Address1 == providerLocation.Address1 &&
                                x.Address2 == providerLocation.Address2 &&
                                x.Address3 == providerLocation.Address3);

                            // it exists by name so we will update it
                            if (exists)
                            {
                               var location = await dbContext.Locations.FirstOrDefaultAsync(x =>
                                        x.Name == providerLocation.Name &&
                                        x.Address1 == providerLocation.Address1 &&
                                        x.Address2 == providerLocation.Address2 &&
                                        x.Address3 == providerLocation.Address3);
                               
                                if (location != null)
                                {
                                    locationGuid = location.LocationGuid;
                                    // not changing locationid or guid, that should stay the same
                                    location.Name = providerLocation.Name;
                                    location.Address1 = providerLocation.Address1;
                                    location.Address2 = providerLocation.Address2;
                                    location.Address3 = providerLocation.Address3;
                                    location.City = providerLocation.City;
                                    location.State = providerLocation.State;
                                    location.Zip = providerLocation.Zip;
                                    location.Phone = providerLocation.Phone;
                                    location.Fax = providerLocation.Fax;
                                    location.IsAcceptingEappointments = providerLocation.IsAcceptingEAppointments;
                                    location.SequenceId = providerLocation.SequenceId;
                                    location.Lat = providerLocation.Lat;
                                    location.Lng = providerLocation.Lng;
                                    //location.Type = providerLocation.type;
                                    location.MainPhone = providerLocation.mainPhone;
                                    location.AdmissionsPhone = providerLocation.admissionsPhone;
                                    location.UpdateDate = DateTime.Now;
                                    location.RecentUser = "UpdateIMPORT";
                                }

                                _logger.WriteInfo("Provider Location being updated " + provider.FirstName + " " + provider.LastName);
                                Console.WriteLine("Provider Location being updated " + provider.FirstName + " " + provider.LastName);
                            }
                            // doesn't exist by name so we will create a new one
                            else
                            {
                                // we ned to generate an id and Guid
                                // get max id + 1
                                locationGuid = Guid.NewGuid().ToString();

                                // add a new location
                                var location = new Location
                                {
                                    LocationId = providerLocation.LocationId,
                                    LocationGuid = locationGuid,
                                    Name = providerLocation.Name,
                                    Address1 = providerLocation.Address1,
                                    Address2 = providerLocation.Address2,
                                    Address3 = providerLocation.Address3,
                                    City = providerLocation.City,
                                    State = providerLocation.State,
                                    Zip = providerLocation.Zip,
                                    Phone = providerLocation.Phone,
                                    Fax = providerLocation.Fax,
                                    IsAcceptingEappointments =
                                        providerLocation.IsAcceptingEAppointments,
                                    SequenceId = providerLocation.SequenceId,
                                    Lat = providerLocation.Lat,
                                    Lng = providerLocation.Lng,
                                    //Type = providerLocation.type,
                                    MainPhone = providerLocation.mainPhone,
                                    AdmissionsPhone = providerLocation.admissionsPhone,
                                    InsertDate = DateTime.Now,
                                    RecentUser = "InsertImport"
                                };
                                _logger.WriteInfo("Provider Location being added " + provider.FirstName + " " + provider.LastName);
                                Console.WriteLine("Provider Location being added " + provider.FirstName + " " + provider.LastName);
                                dbContext.Locations.Add(location);
                            }

                            await dbContext.SaveChangesAsync();
                        }
                        else
                        {
                            // locationid and locationguid are not 0/null so we need to get teh record by that value and update/add it
                            var location = await dbContext.Locations.FirstOrDefaultAsync(x =>
                                x.LocationGuid == providerLocation.LocationGuid);
                            locationGuid = providerLocation.LocationGuid;
                            if (location != null)
                            {
                                location.LocationId = providerLocation.LocationId;
#pragma warning disable CS8601 // Possible null reference assignment.
                                // I suppressed this to avoid making a change in the database but there should never be a null LocationGuid
                                location.LocationGuid = providerLocation.LocationGuid;
#pragma warning restore CS8601 // Possible null reference assignment.
                                location.Name = providerLocation.Name;
                                location.Address1 = providerLocation.Address1;
                                location.Address2 = providerLocation.Address2;
                                location.Address3 = providerLocation.Address3;
                                location.City = providerLocation.City;
                                location.State = providerLocation.State;
                                location.Zip = providerLocation.Zip;
                                location.Phone = providerLocation.Phone;
                                location.Fax = providerLocation.Fax;
                                location.IsAcceptingEappointments = providerLocation.IsAcceptingEAppointments;
                                location.SequenceId = providerLocation.SequenceId;
                                location.Lat = providerLocation.Lat;
                                location.Lng = providerLocation.Lng;
                                // location.Type = providerLocation.type;
                                location.MainPhone = providerLocation.mainPhone;
                                location.AdmissionsPhone = providerLocation.admissionsPhone;
                                location.UpdateDate = DateTime.Now;
                                location.RecentUser = "UpdateIMPORT";

                                _logger.WriteInfo("Provider Location being updated " + provider.FirstName + " " + provider.LastName);
                                Console.WriteLine("Provider Location being updated " + provider.FirstName + " " + provider.LastName);
                            }
                            // add new
                            else
                            {
                                locationGuid = Guid.NewGuid().ToString();
                                // add a new location
                                location = new Location
                                {
                                    LocationId = providerLocation.LocationId,
                                    LocationGuid = locationGuid,
                                    Name = providerLocation.Name,
                                    Address1 = providerLocation.Address1,
                                    Address2 = providerLocation.Address2,
                                    Address3 = providerLocation.Address3,
                                    City = providerLocation.City,
                                    State = providerLocation.State,
                                    Zip = providerLocation.Zip,
                                    Phone = providerLocation.Phone,
                                    Fax = providerLocation.Fax,
                                    IsAcceptingEappointments =
                                        providerLocation.IsAcceptingEAppointments,
                                    SequenceId = providerLocation.SequenceId,
                                    Lat = providerLocation.Lat,
                                    Lng = providerLocation.Lng,
                                    // Type = providerLocation.type,
                                    MainPhone = providerLocation.mainPhone,
                                    AdmissionsPhone = providerLocation.admissionsPhone,
                                    InsertDate = DateTime.Now,
                                    RecentUser = "InsertImport"
                                };
                                _logger.WriteInfo("Provider Location being added " + provider.FirstName + " " + provider.LastName);
                                Console.WriteLine("Provider Location being added " + provider.FirstName + " " + provider.LastName);
                                dbContext.Locations.Add(location);
                            }
                            await dbContext.SaveChangesAsync();
                        }
                        // it exists already (no need to add location)
                        // we need to get location Guid
                        var locMapping = await dbContext.LocationMappings.AnyAsync(x =>
                            x.LocationGuid == locationGuid && x.ProviderGuid == provider.Guid);
                        
                        if (!locMapping)
                        {
                            var loc = new LocationMapping
                            {
                                LocationGuid = locationGuid,
                                ProviderGuid = provider.Guid,
                                IsDefaultLocation = providerLocation.IsDefaultLocation,
                                Type = providerLocation.type
                            };
                            dbContext.LocationMappings.Add(loc);
                        }
                        else
                        {
                            var loc = await dbContext.LocationMappings.FirstOrDefaultAsync(x =>
                                x.LocationGuid == locationGuid &&
                                x.ProviderGuid == provider.Guid);
                            if (loc != null)
                            {
                                loc.IsDefaultLocation = providerLocation.IsDefaultLocation;
                                loc.Type = providerLocation.type;
                            }
                        }

                        if (locationGuid == null)
                        {
                            _logger.WriteError("Location Guid is null");
                            Console.WriteLine("Location Guid is null");
                        }
                        if (locationGuid== "f7fb4a0a-bfa3-4675-98fa-2469b0102ddc") Console.WriteLine("Processing location f7fb4a0a-bfa3-4675-98fa-2469b0102ddc");
                        await dbContext.SaveChangesAsync();
                        if (providerLocation.Hours is {Count: > 0} hours)
                            await ProcessLocationHoursUpdate(hours, locationGuid, dbContext);
                        var providers = providerLocation.OtherProviders;
                        if (providers is {Count: > 0})
                            await ProcessLocationProvidersUpdate(dbContext, provider, providerLocation, locationGuid);
                        var extenders = providerLocation.OfficeExtenders;
                        if (extenders is {Count: > 0})
                            await ProcessLocationOfficeExtendersUpdate(extenders, locationGuid, dbContext);
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving Provider Location" + e.Message);
                        Console.WriteLine("Exception occurred saving Provider Location" + e.Message);
                    }

                    
                }

               
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting location mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting location mapping for provider " +
                                  provider.FirstName + " " +
                                  provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Location:Hours in db
        /// <param name="hours">Provider Guid Identifier</param>
        /// <param name="locationGuid">Location Guid Identifier</param>
        /// </summary>
        private async Task ProcessLocationHoursUpdate(List<MarketingHour> hours, string locationGuid, ProviderDataDBContext dbContext)
        {
            try
            {
                // delete any that don't belong first
                var dbHours = await dbContext.LocationsHours.Where(x => x.LocationGuid == locationGuid).ToListAsync();
                foreach (var hour in dbHours)
                {
                    if (hours.Any(x => x.DayOfWeek == hour.DayOfWeek)) continue;
                    // delete it
                    dbContext.Remove(hour);
                    await dbContext.SaveChangesAsync();
                    _logger.WriteInfo($"Removed Location Hour {hour.DayOfWeek} for {locationGuid}");
                    Console.WriteLine($"Removed Location Hour {hour.DayOfWeek} Updated for {locationGuid}");
                }
                // now add new ones and update existing ones
                foreach (var hour in hours)
                {
                    var exists = await dbContext.LocationsHours.AnyAsync(x =>
                        x.LocationGuid == locationGuid && x.DayOfWeek == hour.DayOfWeek);
                    // if it exists update open and close
                    if (exists)
                    {
                        var locationHours = await dbContext.LocationsHours.FirstAsync(x =>
                            x.LocationGuid == locationGuid && x.DayOfWeek == hour.DayOfWeek);
                        locationHours.CloseTime = hour.CloseTime;
                        locationHours.OpenTime = hour.OpenTime;
                        locationHours.UpdateDate = DateTime.Now;
                        locationHours.RecentUser = "InsertImport";
                        _logger.WriteInfo($"Location Hour {locationHours.DayOfWeek} Updated for {locationGuid}");
                        Console.WriteLine($"Location Hour {locationHours.DayOfWeek} Updated for {locationGuid}");
                        await dbContext.SaveChangesAsync();
                    }
                    // else add
                    else
                    {
                        var newLocationHour = new LocationsHour
                        {
                            DayOfWeek = hour.DayOfWeek,
                            OpenTime = hour.OpenTime,
                            CloseTime = hour.CloseTime,
                            LocationGuid = locationGuid,
                            InsertDate = DateTime.Now,
                            RecentUser = "InsertImport"
                        };
                        dbContext.LocationsHours.Add(newLocationHour);
                        await dbContext.SaveChangesAsync();
                        _logger.WriteInfo($"Location Hour {hour.DayOfWeek} added for {locationGuid}");
                        Console.WriteLine($"Location Hour {hour.DayOfWeek} added for {locationGuid}");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError($"Error Updating/Inserting location hours mapping for location {locationGuid}  {e.Message}");
                Console.WriteLine($"Error Updating/Inserting location hours mapping for location {locationGuid}  {e.Message}");
            }
        }
        /// <summary>
        /// Updates Provider Location:Other Providers in db
        /// <param name="provider">Provider Model</param>
        /// <param name="providerGuid">Provider Guid Identifier</param>
        /// <param name="locationGuid">Location Guid Identifier</param>
        /// </summary>
        private async Task ProcessLocationProvidersUpdate(ProviderDataDBContext db, MarketingProvider provider, MarketingLocation providerLocation, string? locationGuid)
        {
            try
            {
                if (providerLocation.OtherProviders != null)
                    foreach (var providerLocationOtherProvider in providerLocation.OtherProviders)
                    {
                        try
                        {
                            var lp = await db.LocationsProviders.FirstOrDefaultAsync(x => x.LocationGuid == locationGuid && x.ProviderGuid == providerLocationOtherProvider.Guid);

                            if (lp != null)
                            {
                                // get provider guid
                                lp.CommonName = providerLocationOtherProvider.CommonName;
                                lp.UrlRoute = providerLocationOtherProvider.UrlRoute;
                                lp.JobTitle = providerLocationOtherProvider.JobTitle;
                                lp.UpdateDate = DateTime.Now;
                                lp.RecentUser = "UpdateIMPORT";
                            }
                            else
                            {
                                var newLocationOtherProvider = new LocationsProvider
                                {
                                    LocationGuid = locationGuid,
                                    ProviderGuid = providerLocationOtherProvider.Guid,
                                    CommonName = providerLocationOtherProvider.CommonName,
                                    UrlRoute = providerLocationOtherProvider.UrlRoute,
                                    JobTitle = providerLocationOtherProvider.JobTitle,
                                    InsertDate = DateTime.Now,
                                    RecentUser = "InsertImport"
                                };
                                db.LocationsProviders.Add(newLocationOtherProvider);
                            }

                            await db.SaveChangesAsync();
                        }
                        catch (Exception e)
                        {
                            //Logger.Error("Exception saving Other Providers (Rolling back transaction) " + e.Message);
                            _logger.WriteError($"Exception :: Saving Other Providers:: ProviderDataService.ProcessLocationProvidersUpdate :: Details: {e.Message}");
                            throw;
                        }
                    }
            }
            catch (Exception e)
            {
                //Logger.Error(
                //    "Error Updating/Inserting other providers mapping for provider " +
                //    provider.FirstName + " " +
                //    provider.LastName + " Error: " + e.Message);
                _logger.WriteError($"Exception :: Updating/Inserting Other Providers Mapping for provider:: ProviderDataService.ProcessLocationProvidersUpdate :: Provider: {provider.FirstName} {provider.LastName} :: Details: {e.Message}");
                throw;
            }
        }
       
        /// <summary>
        /// Updates Provider Location:Office Extenders in db
        /// <param name="otherProviders">Provider Model</param>
        /// <param name="locationGuid">Location Guid Identifier</param>
        /// </summary>
        private async Task ProcessLocationOfficeExtendersUpdate(List<MarketingSubProvider> otherProviders, string locationGuid, ProviderDataDBContext dbContext)
        {
            try
            {
                // delete any that don't belong first
                var dbExtenders = await dbContext.LocationsOfficeExtenders.Where(x => x.LocationGuid == locationGuid).ToListAsync();
                foreach (var extender in dbExtenders)
                {
                    if (otherProviders.Any(x => x.Guid == extender.ExtenderGuid)) continue;
                    // delete it
                    dbContext.Remove(extender);
                    await dbContext.SaveChangesAsync();
                    _logger.WriteInfo($"Removed Location Extender {extender.ExtenderGuid} for {locationGuid}");
                    Console.WriteLine($"Removed Location Extender {extender.ExtenderGuid} Updated for {locationGuid}");
                }
                // now add new ones and update existing ones
                foreach (var provider in otherProviders)
                {
                    var exists = await dbContext.LocationsOfficeExtenders.AnyAsync(x =>
                        x.LocationGuid == locationGuid && x.ExtenderGuid == provider.Guid);
                    // if it exists update open and close
                    if (exists)
                    {
                        var locationExtender = await dbContext.LocationsOfficeExtenders.FirstAsync(x =>
                            x.LocationGuid == locationGuid && x.ExtenderGuid == provider.Guid);
                        locationExtender.CommonName = provider.CommonName;
                        locationExtender.JobTitle = provider.JobTitle;
                        locationExtender.UrlRoute = provider.UrlRoute;
                        locationExtender.RecentUser = "UpdateIMPORT";
                        locationExtender.UpdateDate = DateTime.Now;
                        _logger.WriteInfo($"Location Extender {provider.Guid} Updated for {locationGuid}");
                        Console.WriteLine($"Location extender {provider.Guid} Updated for {locationGuid}");
                        await dbContext.SaveChangesAsync();
                    }
                    // else add
                    else
                    {
                        var newLocationExtender = new LocationsOfficeExtender()
                        {
                            ExtenderGuid = provider.Guid,
                            CommonName = provider.CommonName,
                            JobTitle = provider.JobTitle,
                            UrlRoute = provider.UrlRoute,
                            LocationGuid = locationGuid,
                            InsertDate = DateTime.Now,
                            RecentUser = "InsertImport"
                        };
                        dbContext.LocationsOfficeExtenders.Add(newLocationExtender);
                        await dbContext.SaveChangesAsync();
                        _logger.WriteInfo($"Location Extender {provider.Guid} added for {locationGuid}");
                        Console.WriteLine($"Location Extender {provider.Guid} added for {locationGuid}");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError($"Error Updating/Inserting location providers mapping for location {locationGuid}  {e.Message}");
                Console.WriteLine($"Error Updating/Inserting location providers mapping for location {locationGuid}  {e.Message}");
            }
        }

        /// <summary>
        /// Updates Provider Board Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessBoardMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // remove any board mapping that doesn't exist in the marketing api
                var dbBoardMapping = await dbContext.BoardMappings.Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var boardMapping in dbBoardMapping.Where(boardMapping => !provider.BoardMapping.Select(x => x.BoardCode).Contains(boardMapping.BoardCode)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing Board Mapping {boardMapping.Text} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing Board Mapping {boardMapping.Text} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(boardMapping);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing Board Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Board Mapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var boardMapping in provider.BoardMapping)
                {
                    try
                    {
                        var bm = await dbContext.BoardMappings.FirstOrDefaultAsync(x =>
                        x.ProviderGuid == provider.Guid && x.BoardCode == boardMapping.BoardCode);

                        if (bm != null)
                        {
                            bm.BoardCode = boardMapping.BoardCode;
                            bm.Text = boardMapping.Text;
                            bm.BoardStatus = boardMapping.BoardStatus;
                            bm.CertificationDate = boardMapping.CertificationDate;
                            bm.RecertificationDate = boardMapping.RecertificationDate;
                            bm.ExpirationDate = boardMapping.ExpirationDate;
                            bm.CertificationNumber = boardMapping.CertificationNumber;
                            bm.UserDefM1 = boardMapping.UserDef_M1;
                            bm.Active = boardMapping.Active;
                            bm.UpdateDate = DateTime.Now;
                            bm.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Board Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Board Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newBoardMapping = new BoardMapping
                            {
                                ProviderGuid = provider.Guid,
                                BoardCode = boardMapping.BoardCode,
                                Text = boardMapping.Text,
                                BoardStatus = boardMapping.BoardStatus,
                                CertificationDate = boardMapping.CertificationDate,
                                RecertificationDate = boardMapping.RecertificationDate,
                                ExpirationDate = boardMapping.ExpirationDate,
                                CertificationNumber = boardMapping.CertificationNumber,
                                UserDefM1 = boardMapping.UserDef_M1,
                                Active = boardMapping.Active,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.BoardMappings.Add(newBoardMapping);
                            _logger.WriteInfo("New Board Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Board Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }

                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception saving BoardMapping" + e.Message + " " + e.InnerException);
                        Console.WriteLine("Exception saving BoardMapping " + e.Message + " " + e.InnerException);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting board mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting board mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider License Mapping in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessLicenseMappingUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // remove any license mapping that no longer exists in the marketing api
                var dbLicenseMapping =
                    await dbContext.LicenseMappings.Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var licenseMapping in dbLicenseMapping.Where(licenseMapping => !provider.LicenseMapping.Select(x => x.LicenseNumber).Contains(licenseMapping.LicenseNumber)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing License Mapping {licenseMapping.LicenseNumber} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing License Mapping {licenseMapping.LicenseNumber} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(licenseMapping);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing LicenseMapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing LicenseMapping for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var licenseMapping in provider.LicenseMapping)
                {
                    try
                    {
                        var lm = await dbContext.LicenseMappings.FirstOrDefaultAsync(x =>
                        x.ProviderGuid == provider.Guid &&
                        x.LicenseNumber == licenseMapping.LicenseNumber);

                        if (lm != null)
                        {
                            lm.LicenseType = licenseMapping.LicenseType;
                            lm.LicenseStatus = licenseMapping.LicenseStatus;
                            lm.State = licenseMapping.State;
                            lm.LicenseNumber = licenseMapping.LicenseNumber;
                            lm.AwardDate = licenseMapping.AwardDate;
                            lm.ExpirationDate = licenseMapping.ExpirationDate;
                            lm.ContactName = licenseMapping.Contact_Name;
                            lm.ContactPhone = licenseMapping.Contact_Phone;
                            lm.ContactFax = licenseMapping.COntact_Fax;
                            lm.ContactEmail = licenseMapping.Contact_Email;
                            lm.LicensureField = licenseMapping.LicensureField;
                            lm.InstitutionName = licenseMapping.Institution_Name;
                            lm.InstitutionContact = licenseMapping.Institution_Contact;
                            lm.InstitutionAddress1 = licenseMapping.Institution_Address1;
                            lm.InstitutionAddress2 = licenseMapping.Institution_Address2;
                            lm.InstitutionCity = licenseMapping.Institution_City;
                            lm.InstitutionState = licenseMapping.Institution_State;
                            lm.InstitutionZip = licenseMapping.Institution_Zip;
                            lm.Active = licenseMapping.Active;
                            lm.UpdateDate = DateTime.Now;
                            lm.RecentUser = "UpdateIMPORT";
                            await dbContext.SaveChangesAsync();
                            _logger.WriteInfo("Provider License Mapping being updated " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider License Mapping being updated " + provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newLicenseMapping = new LicenseMapping
                            {
                                ProviderGuid = provider.Guid,
                                LicenseType = licenseMapping.LicenseType,
                                LicenseStatus = licenseMapping.LicenseStatus,
                                State = licenseMapping.State,
                                LicenseNumber = licenseMapping.LicenseNumber,
                                AwardDate = licenseMapping.AwardDate,
                                ExpirationDate = licenseMapping.ExpirationDate,
                                ContactName = licenseMapping.Contact_Name,
                                ContactPhone = licenseMapping.Contact_Phone,
                                ContactFax = licenseMapping.COntact_Fax,
                                ContactEmail = licenseMapping.Contact_Email,
                                LicensureField = licenseMapping.LicensureField,
                                InstitutionName = licenseMapping.Institution_Name,
                                InstitutionContact = licenseMapping.Institution_Contact,
                                InstitutionAddress1 = licenseMapping.Institution_Address1,
                                InstitutionAddress2 = licenseMapping.Institution_Address2,
                                InstitutionCity = licenseMapping.Institution_City,
                                InstitutionState = licenseMapping.Institution_State,
                                InstitutionZip = licenseMapping.Institution_Zip,
                                Active = licenseMapping.Active,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.LicenseMappings.Add(newLicenseMapping);
                            _logger.WriteInfo("New License Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New License Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            await dbContext.SaveChangesAsync();
                        }
                        
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception occurred saving LicenseMapping" + e.Message + " " + e.InnerException);
                        Console.WriteLine("Exception occurred saving LicenseMapping" + e.Message + " " + e.InnerException);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting license mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting license mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Expertise in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessExpertiseUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // remove any expertise that don't exist in the marketing api
                var dbExpertise = await dbContext.Expertises.Where(x => x.ProviderGuid == provider.Guid).ToListAsync();
                foreach (var e in dbExpertise.Where(e => !provider.Expertise.Select(x => x.Text).Contains(e.Text)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing Expertise {e.Text} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing Expertise {e.Text} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(e);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        _logger.WriteError($"Error removing Expertise for {provider.FirstName} {provider.LastName} :: {ex.Message}");
                        Console.WriteLine($"Error removing Expertise for {provider.FirstName} {provider.LastName} :: {ex.Message}");
                    }
                }
                foreach (var expertise in provider.Expertise)
                {
                    try
                    {
                        var e = await dbContext.Expertises.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid && x.Text == expertise.Text);

                        if (e != null)
                        {
                            e.Text = expertise.Text;
                            e.OrderId = expertise.OrderId;
                            e.UpdateDate = DateTime.Now;
                            e.RecentUser = "UpdateIMPORT";
                            await dbContext.SaveChangesAsync();
                            _logger.WriteInfo("Provider Expertise being updated " + provider.FirstName + " " +
                                        provider.LastName);
                            Console.WriteLine("Provider Expertise being updated " + provider.FirstName + " " +
                                              provider.LastName);
                        }
                        else
                        {
                            var newExpertise = new Expertise
                            {
                                ProviderGuid = provider.Guid,
                                Text = expertise.Text,
                                OrderId = expertise.OrderId,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.Expertises.Add(newExpertise);
                            await dbContext.SaveChangesAsync();
                            _logger.WriteInfo("New Expertise Mapping for Provider being added " + provider.FirstName +
                                        " " + provider.LastName);
                            Console.WriteLine("New Expertise Mapping for Provider being added " + provider.FirstName +
                                              " " + provider.LastName);
                        }

                        
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception saving Expertise" + e.Message + " " + e.InnerException);
                        Console.WriteLine("Exception saving Expertise" + e.Message + " " + e.InnerException);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting expertise mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting expertise mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
            }
        }

        /// <summary>
        /// Updates Provider Provider Paragraphs in db
        /// <param name="provider">Provider Model</param>
        /// </summary>
        private async Task ProcessProviderParagraphsUpdate(MarketingProvider provider, ProviderDataDBContext dbContext)
        {
            try
            {
                // remove any provider paragraphs that don't exist in the marketing api
                var dbParagraphs = await dbContext.ProviderParagraphs.Where(x => x.ProviderGuid == provider.Guid)
                    .ToListAsync();
                foreach (var paragraph in dbParagraphs.Where(paragraph => !provider.ProviderParagraphs.Select(x => x.Text).Contains(paragraph.Text)))
                {
                    try
                    {
                        _logger.WriteInfo($"Removing Paragraph {paragraph.Text} for Provider {provider.FirstName} {provider.LastName}");
                        Console.WriteLine($"Removing Paragraph {paragraph.Text} for Provider {provider.FirstName} {provider.LastName}");
                        dbContext.Remove(paragraph);
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError($"Error removing Paragraphs for {provider.FirstName} {provider.LastName} :: {e.Message}");
                        Console.WriteLine($"Error removing Paragraphs for {provider.FirstName} {provider.LastName} :: {e.Message}");
                    }
                }
                foreach (var providerParagraph in provider.ProviderParagraphs)
                {
                    try
                    {
                        var pp = await dbContext.ProviderParagraphs.FirstOrDefaultAsync(x =>
                            x.ProviderGuid == provider.Guid && x.Text == providerParagraph.Text);

                        if (pp != null)
                        {
                            pp.Text = providerParagraph.Text;
                            pp.OrderId = providerParagraph.OrderId;
                            pp.UpdateDate = DateTime.Now;
                            pp.RecentUser = "UpdateIMPORT";

                            _logger.WriteInfo("Provider Provider Paragraph being updated " +
                                              provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("Provider Provider Paragraph being updated " +
                                              provider.FirstName + " " + provider.LastName);
                        }
                        else
                        {
                            var newProviderParagraph = new ProviderParagraph
                            {
                                ProviderGuid = provider.Guid,
                                Text = providerParagraph.Text,
                                OrderId = providerParagraph.OrderId,
                                InsertDate = DateTime.Now,
                                RecentUser = "InsertImport"
                            };
                            dbContext.ProviderParagraphs.Add(newProviderParagraph);
                            _logger.WriteInfo("New Provider Paragraph Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                            Console.WriteLine("New Provider Paragraph Mapping for Provider being added " + provider.FirstName + " " + provider.LastName);
                        }
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteError("Exception saving Paragraphs " + e.Message + " " + e.InnerException);
                        Console.WriteLine("Exception saving Paragraphs " + e.Message + " " + e.InnerException);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError("Error Updating/Inserting provider paragraphs mapping for provider " +
                             provider.FirstName + " " +
                             provider.LastName + " Error: " + e.Message);
                Console.WriteLine("Error Updating/Inserting provider paragraphs mapping for provider " +
                                   provider.FirstName + " " +
                                   provider.LastName + " Error: " + e.Message);
                throw;
            }
        }
    }
}
