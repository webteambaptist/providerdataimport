﻿using Data.Models;
using LoggingService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProviderDataImport.Services;
using SendMailService.Services;

namespace ProviderDataImport
{
    public class ProviderDataProcessor
    {
        private static readonly LoggerService Logger = new LoggerService("ProviderDataProcessor");
        private static string _to = null!;
        private readonly IConfiguration _config;
        private readonly SendMail _sendMail;
        public ProviderDataProcessor(string to, IConfiguration config)
        {
            _to = to;
            _config = config;
            _sendMail = new SendMail(_config);
        }

        /// <summary>
        /// Processes Provider Data
        /// </summary>
        public async Task ProcessProviderData()
        {
            try
            {
                var connection = _config["ConnectionStrings:DefaultConnection"];
                Console.WriteLine(connection);
                SendStartEmail();

                var processComplete = await ProcessData();

                if (processComplete)
                {
                    //Only send finished email when the processes has completed without any issues. Otherewise one of the exception emails will send.
                    SendCompleteEmail();
                }
                else
                {
                    var error = $"Exception occured ProviderDataProcessor.ProccesData() :: Details:  Issue updating/inserting provider to database.";

                    Logger.WriteError(error);

                    var env = _config["AppSettings:Env"];

                    var subject = $"Povider Data Import Exception ({env})....";
                    await _sendMail.Send(error, _to, null, subject);
                }
            }
            catch (Exception e)
            {
                var error =
                  $"Exception occured ProviderDataProcessor.ProcessProviderData() :: Details: {e.Message} {e.InnerException}";
                Logger.WriteError(error);

                var env = _config["AppSettings:Env"];

                var subject = $"Povider Data Import Exception ({env})....";
                await _sendMail.Send(error, _to, null, subject);
            }
        }

        /// <summary>
        /// Process Data for Import
        /// </summary>
        /// <returns>bool</returns>
        private async Task<bool> ProcessData()
        {
            try
            {
                var dataService = new ImportDataService(_config);
                var providers = await dataService.GetProviders();

                if (providers is {Count: > 0})
                {
                    Logger.WriteInfo("Remove providers starting at:: " + DateTime.Now);
                    Console.WriteLine("Remove providers starting at:: " + DateTime.Now);
                    //Delete Providers 
                    await using var db = new ProviderDataDBContext(_config["ConnectionStrings:DefaultConnection"]);
                    var dbProviders = await db.Providers.ToListAsync();
                    await dataService.RemoveProviders(providers, dbProviders);
                    Logger.WriteInfo("Remove providers finished at:: " + DateTime.Now);
                    Console.WriteLine("Remove providers finished at:: " + DateTime.Now);

                    Logger.WriteInfo("update providers starting at:: " + DateTime.Now);
                    Console.WriteLine("update providers starting at:: " + DateTime.Now);
                    //Import Provider Data
                    await dataService.ImportProviderData(providers);
                    Logger.WriteInfo("====================================================================");
                    Logger.WriteInfo("update providers finished at:: " + DateTime.Now);
                    Console.WriteLine("====================================================================");
                    Console.WriteLine("update providers finished at:: " + DateTime.Now);
                    return true;
                }
                else
                {
                    Logger.WriteError($"Exception :: ProviderDataProcessor::ProccesData() :: Details: There were no providers returned from the microservice.");
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.WriteError($"Exception :: ProcessProviderData::ProcessData() :: Details: {e.Message}");
                return false;
            }
        }

        /// <summary>
        /// Sends out the email that the process has been started
        /// </summary>
        private void SendStartEmail()
        {
            var body = "Provider Data Import Started....";

            var env = _config["AppSettings:Env"];

            var subject = $"Provider Data Import Started ({env})....";
            _sendMail.Send(body, _to, null, subject);
        }

        /// <summary>
        /// Sends out the email that the process has been completed.
        /// </summary>
        private void SendCompleteEmail()
        {
            var env = _config["AppSettings:Env"];

            var subject = $"Provider Data Import Finished ({env})....";
            var body = "Provider Data Import Finished....";
            _sendMail.Send(body, _to, null, subject);
        }
    }
}
