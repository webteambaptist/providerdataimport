﻿using LoggingService;
using Microsoft.Extensions.Configuration;
using ProviderDataImport;
using ProviderDataImport.Services;
using SendMailService.Services;

var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
Console.WriteLine(environment);
var config = new ConfigurationBuilder()
    .AddJsonFile($"appsettings.json", true, true)
    .AddJsonFile($"appsettings.{environment}.json", true, true)
    .AddEnvironmentVariables().Build();

//// Get values from the config given their key and their target type.
var sender = config["AppSettings:Sender"];
var to = config["AppSettings:Recipient"];
var version = config["AppVersion"];
var sendMail = new SendMail(config);
var logger = new LoggerService("Log");
var env = config["AppSettings:Env"];

try
{
    Console.WriteLine($"Running application version: {version}");
    Console.WriteLine($"Environment: {environment}");
    logger.WriteInfo($"Environment: {environment}");
    var providerDataProcessor = new ProviderDataProcessor(to, config);

    await providerDataProcessor.ProcessProviderData();

    Environment.Exit(0);
}
catch (Exception e)
{
    var error =
        $"Exception occurred while running Program.Main() {Environment.NewLine} ex.Message: {e.Message} {Environment.NewLine} InnerException :: {e.InnerException} {Environment.NewLine} ";
    logger.WriteError(error);

    var subject = $"Provider Data Import Process Failed ({env})";
    await sendMail.Send(error, to, null, subject);

    Environment.Exit(0);
}