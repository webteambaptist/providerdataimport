﻿namespace ProviderDataImport.AppModels
{
    public class MarketingBoardMapping
    {
        public string BoardCode { get; set; } = null!;
        public string Text { get; set; } = null!;
        public string BoardStatus { get; set; } = null!;
        public string CertificationDate { get; set; } = null!;
        public string RecertificationDate { get; set; } = null!;
        public string ExpirationDate { get; set; } = null!;
        public string CertificationNumber { get; set; } = null!;
        public string UserDef_M1 { get; set; } = null!;
        public bool Active { get; set; }
    }
}
