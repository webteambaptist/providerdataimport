﻿namespace ProviderDataImport.AppModels
{
    public class MarketingSpecialtyMapping
    {
        public string SpecialtyCode { get; set; } = null!;
        public string Text { get; set; } = null!;
        public string JobTitle { get; set; } = null!;
        public string SpecialtyStatus { get; set; } = null!;
        public string SpecialtyType { get; set; } = null!;
        public string HippaTaxonomy { get; set; } = null!;
        public bool IsBoardCertified { get; set; }
        public string CertificationCode { get; set; } = null!;
        public string CertifyingBoardName { get; set; } = null!;
        public string BoardCertification { get; set; } = null!;
        public string BoardDescription { get; set; } = null!;
        public int SpecialtyOrder { get; set; }
        public bool IsPrimary { get; set; }
    }
}
