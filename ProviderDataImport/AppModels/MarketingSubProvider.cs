﻿namespace ProviderDataImport.AppModels
{
    public class MarketingSubProvider
    {
        public string Guid { get; set; } = null!;
        public string CommonName { get; set; } = null!;
        public string UrlRoute { get; set; } = null!;
        public string JobTitle { get; set; } = null!;
    }
}
