﻿namespace ProviderDataImport.AppModels
{
    public class MarketingSuffixMapping
    {
        public string SuffixCode { get; set; } = null!;
        public int OrderId { get; set; }
    }
}
