﻿namespace ProviderDataImport.AppModels
{
    public class MarketingLanguageMapping
    {
        public string LanguageCode { get; set; } = null!;
        public string Text { get; set; } = null!;
    }
}
