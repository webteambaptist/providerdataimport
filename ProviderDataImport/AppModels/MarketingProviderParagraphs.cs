﻿namespace ProviderDataImport.AppModels
{
    public class MarketingProviderParagraphs
    {
        public string Text { get; set; } = null!;
        public string OrderId { get; set; } = null!;
    }
}
