﻿namespace ProviderDataImport.AppModels
{
    public class MarketingPatientFormsMapping
    {
        public string Name { get; set; } = null!;
        public string Type { get; set; } = null!;
        public string Link { get; set; } = null!;
        public string Description { get; set; } = null!;
        public int OrderId { get; set; }
    }
}
