﻿namespace ProviderDataImport.AppModels
{
    public class MarketingHour
    {
        public string DayOfWeek { get; set; } = null!;
        public string OpenTime { get; set; } = null!;
        public string CloseTime { get; set; } = null!;
    }
}
