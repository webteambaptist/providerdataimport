﻿namespace ProviderDataImport.AppModels
{
    public class AccessToken
    {
        public string access_token { get; set; } = null!;
        public string token_type { get; set; } = null!;
    }
}
