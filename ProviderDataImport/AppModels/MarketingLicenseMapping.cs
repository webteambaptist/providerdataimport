﻿namespace ProviderDataImport.AppModels
{
    public class MarketingLicenseMapping
    {
        public string LicenseType { get; set; } = null!;
        public string LicenseStatus { get; set; } = null!;
        public string State { get; set; } = null!;
        public string LicenseNumber { get; set; } = null!;
        public string AwardDate { get; set; } = null!;
        public string ExpirationDate { get; set; } = null!;
        public string Contact_Name { get; set; } = null!;
        public string Contact_Phone { get; set; } = null!;
        public string COntact_Fax { get; set; } = null!;
        public string Contact_Email { get; set; } = null!;
        public string LicensureField { get; set; } = null!;
        public string Institution_Name { get; set; } = null!;
        public string Institution_Contact { get; set; } = null!;
        public string Institution_Address1 { get; set; } = null!;
        public string Institution_Address2 { get; set; } = null!;
        public string Institution_City { get; set; } = null!;
        public string Institution_State { get; set; } = null!;
        public string Institution_Zip { get; set; } = null!;
        public bool Active { get; set; }
    }
}
