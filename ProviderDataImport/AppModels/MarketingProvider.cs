﻿namespace ProviderDataImport.AppModels
{
    public class MarketingProvider
    {
        public int Id { get; set; }
        public string Guid { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string MiddleName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string FormattedCommonName { get; set; } = null!;
        public string FormattedLastNameFirst { get; set; } = null!;
        public string EchoPhysicianId { get; set; } = null!;
        public string EchoDoctorNumber { get; set; } = null!;
        public string NationalId { get; set; } = null!;
        public string ContactPhone { get; set; } = null!;
        public string ContactCellPhone { get; set; } = null!;
        public string ContactPager { get; set; } = null!;
        public string GenderId { get; set; } = null!;
        public string DoctorImage { get; set; } = null!;
        public string DoctorHeaderImage { get; set; } = null!;
        public string ThumbnailPhoto { get; set; } = null!;
        public string EchoSuffix { get; set; } = null!;
        public string UrlRoute { get; set; } = null!;
        public string FormattedUrlRoute { get; set; } = null!;
        public bool IsAcceptingNewPatients { get; set; }
        public bool IsAcceptingNewPatientsVisible { get; set; }
        public int TypeId { get; set; }
        public string Introduction { get; set; } = null!;
        public string FormattedSpecialties { get; set; } = null!;
        public bool IsArchived { get; set; }
        public bool IsHide { get; set; }
        public string NameOverride { get; set; } = null!;
        public string JobTitleOverride { get; set; } = null!;
        public string FormattedJobTitle { get; set; } = null!;
        public string LegalPracticeName { get; set; } = null!;
        public bool DoesAcceptsEapps { get; set; }
        public bool IsPcmh { get; set; }
        public bool IsBloodless { get; set; }
        public bool AdminApproved { get; set; }
        public bool IsNewDoctor { get; set; }
        public bool IsBpp { get; set; }
        public bool IsEmployed { get; set; }
        public bool DoesHavePatientReviews { get; set; }
        public string PublicationQuery { get; set; } = null!;
        public string LastUpdateDateTime { get; set; } = null!;
        public string LastUpdateUser { get; set; } = null!;
        public MarketingType Type { get; set; } = null!;
        public List<MarketingGroupsMapping> GroupsMapping { get; set; } = null!;
        public List<MarketingSpecialtyMapping> SpecialtyMapping { get; set; } = null!;
        public List<MarketingLanguageMapping> LanguageMapping { get; set; } = null!;
        public List<MarketingEducationMapping> EducationMapping { get; set; } = null!;
        public List<MarketingSuffixMapping> SuffixMapping { get; set; } = null!;
        public List<MarketingHospitalMapping> HospitalMapping { get; set; } = null!;
        public List<MarketingAgesTreatedMapping> AgesTreatedMappings { get; set; } = null!;
        public List<MarketingPatientFormsMapping> PatientFormsMappings { get; set; } = null!;
        public List<MarketingLocation> Locations { get; set; } = null!;
        public List<MarketingCredentialMapping> CredentialMapping { get; set; } = null!;
        public List<MarketingBoardMapping> BoardMapping { get; set; } = null!;
        public List<MarketingLicenseMapping> LicenseMapping { get; set; } = null!;
        public List<MarketingExpertise> Expertise { get; set; } = null!;
        public List<MarketingProviderParagraphs> ProviderParagraphs { get; set; } = null!;
    }

}
