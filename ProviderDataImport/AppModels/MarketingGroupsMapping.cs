﻿namespace ProviderDataImport.AppModels
{
    public class MarketingGroupsMapping
    {
        public string Code { get; set; } = null!;
        public string Text { get; set; } = null!;
        public string Type { get; set; } = null!;
        public int OrderId { get; set; }
        public bool IsPrimary { get; set; }
    }
}
