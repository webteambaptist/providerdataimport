﻿namespace ProviderDataImport.AppModels
{
    public class MarketingCredentialMapping
    {
        public string Code { get; set; } = null!;
        public string Text { get; set; } = null!;
        public string CertificationType { get; set; } = null!;
        public string LicenseNumber { get; set; } = null!;
        public string LicenseDescription { get; set; } = null!;
        public string IssueDate { get; set; } = null!;
        public string ExpirationDate { get; set; } = null!;
        public string BoardName { get; set; } = null!;
        public string Address1 { get; set; } = null!;
        public string Address2 { get; set; } = null!;
        public string City { get; set; } = null!;
        public string State { get; set; } = null!;
        public string Zip { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Fax { get; set; } = null!;
        public bool IsInactive { get; set; }
    }
}
