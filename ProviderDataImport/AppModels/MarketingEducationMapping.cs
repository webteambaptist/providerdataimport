﻿namespace ProviderDataImport.AppModels
{
    public class MarketingEducationMapping
    {
        public string DegreeCode { get; set; } = null!;
        public string DegreeText { get; set; } = null!;
        public string ProgramCode { get; set; } = null!;
        public string ProgramText { get; set; } = null!;
        public string InsitutionCode { get; set; } = null!;
        public string InstitutionText { get; set; } = null!;
        public string InstitutionCity { get; set; } = null!;
        public string InstitutionState { get; set; } = null!;
        public string Graduate_Complete { get; set; } = null!;
        public string City { get; set; } = null!;
        public string State { get; set; } = null!;
        public string TrainingDescription { get; set; } = null!;
        public string StartYear { get; set; } = null!;
        public string EndYear { get; set; } = null!;
        public string SequenceId { get; set; } = null!;
    }
}
