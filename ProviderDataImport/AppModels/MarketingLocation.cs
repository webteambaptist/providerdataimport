﻿namespace ProviderDataImport.AppModels
{
    public class MarketingLocation
    {
        public int LocationId { get; set; }
        public string LocationGuid { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string Address1 { get; set; } = null!;
        public string Address2 { get; set; } = null!;
        public string Address3 { get; set; } = null!;
        public string City { get; set; } = null!;
        public string State { get; set; } = null!;
        public string Zip { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Fax { get; set; } = null!;
        public bool IsAcceptingEAppointments { get; set; }
        public bool IsDefaultLocation { get; set; }
        public int SequenceId { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string type { get; set; } = null!;
        public string mainPhone { get; set; } = null!;
        public string admissionsPhone { get; set; } = null!;
        public List<MarketingHour> Hours { get; set; } = null!;
        public List<MarketingSubProvider> OtherProviders { get; set; } = null!;
        public List<MarketingSubProvider> OfficeExtenders { get; set; } = null!;
    }
}
