﻿namespace ProviderDataImport.AppModels
{
    public class MarketingExpertise
    {
        public string Text { get; set; } = null!;
        public string OrderId { get; set; } = null!;
    }
}
