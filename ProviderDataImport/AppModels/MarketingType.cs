﻿namespace ProviderDataImport.AppModels
{
    public class MarketingType
    {
        public int Id { get; set; }
        public string Code { get; set; } = null!;
        public string Text { get; set; } = null!;
    }
}
