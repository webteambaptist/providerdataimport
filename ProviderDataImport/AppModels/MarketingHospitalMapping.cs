﻿namespace ProviderDataImport.AppModels
{
    public class MarketingHospitalMapping
    {
        public string HospitalCode { get; set; } = null!;
        public string HospitalName { get; set; } = null!;
        public string StaffCode { get; set; } = null!;
        public string OrderId { get; set; } = null!;
    }
}
