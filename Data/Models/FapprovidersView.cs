﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class FapprovidersView
    {
        public int Id { get; set; }
        public string Guid { get; set; } = null!;
        public string? FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
        public string? DoctorImage { get; set; }
        public string? FormattedJobTitle { get; set; }
        public string? FormattedCommonName { get; set; }
        public string? EchoPhysicianId { get; set; }
        public string? EchoDoctorNumber { get; set; }
        public string? GenderId { get; set; }
        public string? EchoSuffix { get; set; }
        public bool? IsBpp { get; set; }
        public bool? IsAcceptingNewPatients { get; set; }
        public string? LegalPracticeName { get; set; }
        public string? HospitalCodes { get; set; }
        public string? SpecialtyCodes { get; set; }
        public string? LanguageCodes { get; set; }
        public string? OtherAddressIds { get; set; }
        public string? AgesTreatedText { get; set; }
    }
}
