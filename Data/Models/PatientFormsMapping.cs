﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class PatientFormsMapping
    {
        public int Id { get; set; }
        public string ProviderGuid { get; set; } = null!;
        public string? Name { get; set; }
        public string? Type { get; set; }
        public string? Link { get; set; }
        public string? Description { get; set; }
        public int? OrderId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string? RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; } = null!;
    }
}
