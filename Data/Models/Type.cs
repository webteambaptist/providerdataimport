﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Type
    {
        public int IdKey { get; set; }
        public string ProviderGuid { get; set; } = null!;
        public int? Id { get; set; }
        public string? Code { get; set; }
        public string? Text { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string? RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; } = null!;
    }
}
