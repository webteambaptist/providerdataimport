﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class LocationMapping
    {
        public int Id { get; set; }
        public string LocationGuid { get; set; } = null!;
        public string ProviderGuid { get; set; } = null!;
        public bool IsDefaultLocation { get; set; }
        public string? Type { get; set; }

        public virtual Location LocationGu { get; set; } = null!;
        public virtual Provider ProviderGu { get; set; } = null!;
    }
}
