﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class BoardMapping
    {
        public int Id { get; set; }
        public string ProviderGuid { get; set; } = null!;
        public string? BoardCode { get; set; }
        public string? Text { get; set; }
        public string? BoardStatus { get; set; }
        public string? CertificationDate { get; set; }
        public string? RecertificationDate { get; set; }
        public string? ExpirationDate { get; set; }
        public string? CertificationNumber { get; set; }
        public string? UserDefM1 { get; set; }
        public bool? Active { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string? RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; } = null!;
    }
}
