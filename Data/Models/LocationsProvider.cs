﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class LocationsProvider
    {
        public int Id { get; set; }
        public string LocationGuid { get; set; } = null!;
        public string ProviderGuid { get; set; } = null!;
        public string? CommonName { get; set; }
        public string? UrlRoute { get; set; }
        public string? JobTitle { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string? RecentUser { get; set; }

        public virtual Location LocationGu { get; set; } = null!;
    }
}
