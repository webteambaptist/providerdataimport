﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Hospital
    {
        public int Id { get; set; }
        public string? HospitalCode { get; set; }
        public string? HospitalName { get; set; }
        public string? StaffCode { get; set; }
        public string? OrderId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string? RecentUser { get; set; }
    }
}
