﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class LocationsHour
    {
        public int Id { get; set; }
        public string LocationGuid { get; set; } = null!;
        public string? DayOfWeek { get; set; }
        public string? OpenTime { get; set; }
        public string? CloseTime { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string? RecentUser { get; set; }

        public virtual Location LocationGu { get; set; } = null!;
    }
}
